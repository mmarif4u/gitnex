package org.mian.gitnex.models;

/**
 * Author M M Arif
 */

public class CreateLabel {

    private String name;
    private String color;

    public CreateLabel(String name, String color) {
        this.name = name;
        this.color = color;
    }

}
